// alert("file linked");

// //Number
//     var num1=35;
//     var num2=40;
//     alert(num1+num2);//output is 75
//     alert("my favourite numbers is "+ num1);//my favourite numbers is 35
// //camel case
//     var myFavouriteNumber;

// //partial case
//     var MyFavouriteNumber;

// //underscore
//     var my_favourite_number;


// //String
//     var num3="35";
//     var num4="40";
//     alert(num3+num4);//output is  3540

// //Array
//     var colors = ['red', 'blue', 'green'];
//     alert(colors); // red,blue,green
//     alert(colors[1]); // blue
//     alert(colors[3]) //undefined

//     //another way to define arrays
//     var colors1 = new Array ('red', 'blue', 'orange');
//     alert(colors1[2]) // orange

//     colors1.push('purple');
//     alert(colors1[3]);

    // var numbers = [7, 55, 4, 'Seven', 1, 66, 8, 15];

    // alert(numbers[0] + numbers[2]); // 11
    // alert(numbers[3]); // Seven
    // alert(numbers.length);
    // alert(numbers.sort());
    //  alert(numbers.reverse());

// //Object
//     //object literal
//     var person = {
//         firstName : 'Laila',
//         lastName : 'Atty',
//         age : 25,
//         children : ['Farida'], //array
//         address : {
//             houseNum :166,
//             street : 'Erich Salomonstraat',
//             postalCode : '1087EH',
//             city : 'Amsterdam',
//             state : 'Netherlands'
//         },

//         fullName : function() {
//             return this.firstName + " " + this.lastName;
//         }

//     }
//     console.log(person);
//     console.log(person.fullName());
//     console.log(person.address);
//     console.log(person.children[0]);

//     //object constructor
//     var apple = new Object();
//     apple.color = 'red';
//     apple.shape = 'round';
//     apple.description = function() {
//         return 'An apple its color is  ' + this.color +  ' and its shape is ' + this.shape;
//     };
//     console.log(apple);
//     console.log(apple.description());

    // //constructor pattern
    // function Fruit( name, color, shape ) {
    //     this.name = name;
    //     this.color = color;
    //     this.shape = shape;

    //     this.description = function() {
    //         return 'An ' + this.name + ' its color is  ' + this.color +  ' and its shape is ' + this.shape;
    //     }
    // }

    // var apple  = new Fruit('apple', 'red', 'round');
    // var melon = new Fruit('melon', 'green', 'round');

    // console.log(apple);
    // console.log(melon.shape);
    // console.log(melon.description());

    //a raise up object
    // var users = [
    //     {
    //         name: 'Laila',
    //         age: 25
    //     },
    //     {
    //         name: 'Hameed',
    //         age: 28
    //     },
    //     {
    //         name: 'Farida',
    //         age: 1
    //     }
    // ];
    // console.log(users);
// //Loops
//     // for loop
//         for(var i = 0; i <= 10; i ++){
//             console.log(i);
//         }
//     //while loop
//         var j = 0;
//         while( j <= 10){
//             console.log(j);
//             j++;
//         }
//     // forEach
//         var numbers = [ 33, 55, 78, 23, 44, 45];
//         numbers.forEach(function(number) {
//             console.log(number);
//         });

//         numbers.reverse();
//         for(var i = 0; i < numbers.length; i++){
//             console.log(numbers[i]);
//         }

// //Conditionals
//     //if conditions
//     var x = 0;
//     if( x = 1 ){ // assign the right one to the left one
//         console.log("This is true!");
//     }
//     if( 1 == '1' ){ // compare values of the left one and the right one
//         console.log("This is true!");
//     }
//     if( 1 === 1 ){ // compare the values and the datatype
//         console.log("This is true!");
//     }

//     var var1 = 1;
//     var var2 = 2;

//     if(var1 != var2){// not equals to
//         console.log("This is true!");

//     } else {
//         console.log("This is false!");
//     }

//     //switch statement
//     var fruit = 'apple';

//     switch(fruit) {
//         case 'banana':
//             console.log("I hate bananas");
//             break;
//         case 'apple':
//             console.log("I love apples");
//             break;
//         case 'orange':
//             console.log("orange is ok");
//             break;
//         default:
//             console.log("I love all other fruits");
//             break;
//     }

// //Events
//     function doClick(){
//         alert("You clicked");
//     }
//     function changeText(){
//         var heading = document.getElementById('heading');
//         heading.innerHTML = "You clicked";
//     }
//     function showDate(){
//         var time = document.getElementById("time");
//         time.innerHTML = Date();
//     }
//     function clearDate(){
//         var time = document.getElementById("time");
//         time.innerHTML = "Show date";
//     }

//Forms 
    function changeBackground(x){
        var heading = document.getElementById('heading');
        heading.style.color = x.value;
        console.log(x);
    }
    function validateForm(){
        var firstName = document.forms["myForm"]["firstName"].value;
        if(firstName == null || firstName == ""){
            alert("First name is required!");
            return false;
        }
        if(firstName.length < 3){
            alert("First name should be at least 3 characters");
            return false; 
        }

    }








