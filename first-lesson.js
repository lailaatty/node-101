// define variables in javascript
// no types
name = 'asdalsdlas';
var name = 'asdalsdlas';
let nameWithLet = 'new value';
const nameA = {
    prop: 'value';
};


// string 'asdalsdlas'
// number 923827349.8923 12312312312 number('123123123123123') 1.999999999999999999998
// function function () {}
// object {} var obj = {};
// array []
// undefined
// null
// boolean true, false


// object

howItWorksInJs('number') // pragraph 200 - 500 character description for the type
howItWorksInJs('array') // pragraph 200 - 500 character description for the type




// scope
// ..........
var name = 'hameed';

function sayHi(name) {
    var newName = name;
    console.log(newName);

    function () {
        // new scope
    }
}

if (true) {
    // var foo = "dasdasd"
    let foo = "dasdasd"
}

console.log(foo);
// ..........


// housting
{
    var name = undefined;
    name = 'hameed';

    var sayHi = ...;
    var sayHi = function...
        {
            var newName = undefined;
                newName = undefined;
            console.log(newName);
        }
}










// -----------
// es5
// -----------


var Counter = function (wrapperSelector, initValue) {
    var self = this;

    this.number = initValue || 0;
    this.wrapperEl = document.querySelector(wrapperSelector)
    this.countEl = this.wrapperEl.querySelector('.count');
    this.increament = function () {
        this.countEl.innerText++;
    };
    this.decreament = function () {
        this.countEl.innerText--;
    };

    this.increamentEl = this.wrapperEl.querySelector('.increament');
    this.decreamentEl = this.wrapperEl.querySelector('.decreament');

    this.countEl.innerText = this.number;

    this.increamentEl.addEventListener("click", function () {
        self.increament();
    });

    this.decreamentEl.addEventListener("click", function () {
        self.decreament();
    });
}

// var counter1 = new Counter('.wrapper-count-1', 20);
// var counter2 = new Counter('.wrapper-count-2', 0);
// var counter3 = new Counter('.wrapper-count-3', -4434);
// var counter4 = new Counter('.wrapper-count-4', 44);
// var counter5 = new Counter('.wrapper-count-5', 3);


var attachCounters = function (arr) {

    arr.forEach(function(element) {
        new Counter(element.selector,element.initValue);
    });
}


attachCounters([
    {
        selector: ".wrapper-count-1",
        initValue: 20
    },
    {
        selector: ".wrapper-count-2",
        initValue: 0
    },
    {
        selector: ".wrapper-count-3",
        initValue: 776
    },
    {
        selector: ".wrapper-count-4",
        initValue: -9756
    },
    {
        selector: ".wrapper-count-5",
        initValue: 5
    }
]);



// OOP - Object oriented programming
// var tic = {
//     number: 0,
//     increament: function() {},
//     decreament: function() {}
// };

// tic.increament = function () {
//     this.number = this.number + 1;
// };

// tic.decreament = function () {
//     this.number = this.number - 1;
// };


// -----------
// es5+
// -----------

// let number = 0;

// const increament = () => {
//     number = number + 1;
// };

// const decreament = () => {
//     number = number - 1;
// };

// --------------





