var types = {
    "string": "In any computer programming language, a string is a sequence of characters used to represent text. In JavaScript, a String is one of the primitive values and the String object is a wrapper around a String primitive.",
    "array": "arr",
    "number": "In JavaScript, all numbers are implemented in double-precision 64-bit binary format IEEE 754 (i.e. a number between -(253 -1) and 253 -1). There is no specific type for integers. In addition to being able to represent floating-point numbers, the number type has three symbolic values: +Infinity, -Infinity, and NaN (not-a-number). You can use four types of number literals: decimal, binary, octal, and hexadecimal.",
    "function": "fun",
    "object": "Object refers to a data structure containing data and instructions for working with the data. Objects sometimes refer to real-world things, for example a car or map object in a racing game. JavaScript, Java, C++, Python, and Ruby are examples of object-oriented programming languages.",
    "undefined":"A variable that has not been assigned a value has the value undefined.",
    "boolean":"Boolean represents a logical entity and can have two values: true, and false.",
    "Symbol":"Symbols are new to JavaScript in ECMAScript Edition 6. A Symbol is a unique and immutable primitive value and may be used as the key of an Object property (see below). In some programming languages, Symbols are called atoms.)",
    "Null":"The Null type has exactly one value: null."
};



var howItWorksInJs = function (name) {
    if (typeof name !== "string" || name === "") {
        return 'paramter is missing';
    } else if (types[name]){
        return types[name];
    } else {
        return "I don't have any information about " + name;
    }
}
