var result = 0;
var calcOperator = "";
var firstNum = "";
var secondNum = "";
var wrapperEl = document.querySelector('.wholeCalc');

var displayEl = wrapperEl.querySelector('.calcInput');
var buttonsEl = wrapperEl.querySelector('.buttons');


function EqualOp (num1, operation, num2) {
    switch(operation){
        case "+" : {
            return (parseFloat(num1,10) + parseFloat(num2,10));
        } case "-" : {
            return (parseFloat(num1,10) - parseFloat(num2,10));
        } case "*" : {
            return (parseFloat(num1,10) * parseFloat(num2,10));
        } case "/" : {
            if(parseFloat(num2,10) === 0) {
                return "Can't divide on 0" ;
            }
            return (parseFloat(num1,10) / parseFloat(num2,10));
        } case "": {
            return num1;
        }
    }
}

function clearOp () {
    displayEl.value = "";
    firstNum = "";
    calcOperator = "";
    secondNum = "";
}


function fetchInput (input) {
    if(input === "=") {
        displayEl.value = EqualOp(firstNum, calcOperator, secondNum);
        firstNum = displayEl.value;
        calcOperator = "";
        secondNum = "";
    } else if(input === "c") {
        clearOp ();
    } else if(input === "+" || input === "-" || input === "*" || input === "/") {
        calcOperator = input;
    } else if(calcOperator == "") {
        firstNum += input;
        displayEl.value = firstNum;
    } else {
        displayEl.value = "";
        secondNum += input;
        displayEl.value = secondNum;
    }
}



buttonsEl.addEventListener("click", function (event) {
    if(event.target.value) {
        fetchInput(event.target.value);
    }
});

