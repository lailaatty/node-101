//(function (){
var tasksList = [];

function createTask(text ) {
    return {
        name: text,
        id: new Date().getTime(),
        isCompleted: ""
    };
};

//save the list to local storage
function updateLocalStorage(tasksList) {
    //set to local storage
    localStorage.setItem('tasksList', JSON.stringify(tasksList));
    return ;
}

//fetch tasks from local storage
function fetchTasksList() {
    //if the local storage contains tasks
    if (localStorage.getItem('tasksList')) {
        //return tasks
        return JSON.parse(localStorage.getItem('tasksList'));
    }
    //else return null
    return ;
}

// display single task in the web page
function displayTask(task) {
    var name = task.name;
    var id = task.id;
    var isCompleted = task.isCompleted;
    return  '<li>'+
                '<div class="single-element">'+
                        '<label>'+
                            '<input  id="' + id + '" type="checkbox" onchange="completeTask(' + id + ')" ' + isCompleted + '>'+
                            '<span for="checkbox"></span>'+
                            '<label id="task-name">' + name + '</label>' +
                        '</label>'+
                        '<button id="destroy-task" onclick="deleteTask('+id+')" >x</button>'+
                '</div>'+
            '</li>';
}

// display the number of active tasks
function itemNumbers() {
    var count = 0;
    for (var i = 0; i < tasksList.length; i++) {
        //count active tasks
        if(tasksList[i].isCompleted === "" ){
            count++;
        }
    }
    //display num of active tasks in the label
    document.getElementById("items-num").innerHTML = count + " Items Left";
}

// display the list in the web page
function displayTheList(List) {
    var listElement = document.getElementById('list-elements');
    var tasksDom = '';
    for(var i = 0; i < List.length; i++) {
        tasksDom += displayTask(List[i]);
    }
    listElement.innerHTML = tasksDom;
    itemNumbers();
}


// update the taskList array and display it
function updateList() {
    //fetch the list from the local storage
    if (fetchTasksList()) {
        tasksList = fetchTasksList();
        displayTheList(tasksList);
    }
}

//document.getElementById("destroy-task").onclick();

// delete task from the array and update the local storage
function deleteTask(id) {
    //search for the task id and delete it
    for (var i = 0; i < tasksList.length; i++) {
        if(tasksList[i].id === id) {
            tasksList.splice(i,1);
        }
    }
    // delete the task from the local storage
    updateLocalStorage(tasksList);
    // display the updated list
    updateList();
}

//document.getElementById("check-task").onchange
//mark/unmark the task
function completeTask(id) {
        for (var i = 0; i < tasksList.length; i++) {
            if (tasksList[i].id === id) {
                // uncomplete the task
                if (tasksList[i].isCompleted === "checked") {
                    tasksList[i].isCompleted = "";
                }
                //complete the task
                else {
                    tasksList[i].isCompleted = "checked";
                }
            }
        }
    // save and display updating
    updateLocalStorage(tasksList);
    updateList();
}

document.getElementById("clear-completed").onclick = function clearCompleted() {
    //search for checked tasks
    for (var i = 0; i < tasksList.length; i++) {
        if (tasksList[i].isCompleted === "checked") {
            //removing comleted tasks
            tasksList.splice(i, 1);
            //decrease the counter because we deleted an element
            i--;
        }
    }
    // delete the task from the local storage
    updateLocalStorage(tasksList);
    // display the updated list
    updateList();
}

// add new task to the list
document.getElementById("button-enter").onclick = function addNewTask() {
    //fetch the enterd value
    var taskName = document.getElementById("todo-input").value;
    //check if the value is not empty
    if(!taskName) {
        return;
    }
    //create new task {name,id,iscompleted}
    var task = createTask(taskName);
    //add to array
    tasksList.push(task);
    //save the list to local storage
    updateLocalStorage(tasksList);
    //display all tasks that saved in local storage
    displayTheList(tasksList);
    //reset the input value
    document.getElementById("todo-input").value = "";
}

//display all tasks
document.getElementById("all-button").onclick = function displayAllTasks() {

    displayTheList(tasksList);
}

//Display only active tasks
document.getElementById("active-button").onclick = function displayActiveTasks() {
    var activeTasks = [];
    //search for the uncompleted tasks
    for (var i = 0; i < tasksList.length; i++) {
        if (tasksList[i].isCompleted === "") {
            activeTasks.push(tasksList[i]);
        }
    }
    //display active tasks
    displayTheList(activeTasks);
}

document.getElementById("completed-button").onclick = function displayCompletedTasks() {
    var completedTasks = [];
    //search for the completed tasks
    for (var i = 0; i < tasksList.length; i++) {
        if (tasksList[i].isCompleted === "checked") {
            completedTasks.push(tasksList[i]);
        }
    }
    //display completed tasks
    displayTheList(completedTasks);
}

document.getElementById("mark-all").onclick =  function markUnmarkAll() {
    var flag = document.getElementById("mark-all").flag;
    console.log(flag);
    if(flag) {
        for (var i = 0; i < tasksList.length; i++) {
            tasksList[i].isCompleted = "";
        }
        flag = false;

    } else {
        for (var i = 0; i < tasksList.length; i++) {
            tasksList[i].isCompleted = "checked";
        }
        flag = true;
    }
    document.getElementById("mark-all").flag = flag;
    //save the list to local storage
    updateLocalStorage(tasksList);
    //display all tasks that saved in local storage
    displayTheList(tasksList);
}

//})();
