// listen for form submit
document.getElementById('myForm').addEventListener('submit', saveBookmark);
 // save bookmark
function saveBookmark(e){

    //getting submitted values
    var siteName = document.getElementById('siteName').value;
    var siteURL = document.getElementById('siteURL').value;

    var validationResult = validateForm(siteName, siteURL);
    if(validationResult.length) {
        alert(validationResult.join('\n'));
        e.preventDefault();
        return;
    }

    var bookmark = {
        name: siteName,
        url : siteURL
    }
    /*
        //Local storage test
        localStorage.setItem('test', 'hello World');
        console.log(localStorage.getItem('test'));
        localStorage.removeItem('test');
        console.log(localStorage.getItem('test'));
        //localStorage.setItem('test2', 'hello everybody');
    */

    //  Test if bookmarks is null
    if(localStorage.getItem('bookmarks') === null) {
        //init bookmarks
        var bookmarks = [];
        //add to array
        bookmarks.push(bookmark);
        //set to local storage
        localStorage.setItem('bookmarks', JSON.stringify(bookmarks));
    } else {
        //get bookmarks from local storage
        var bookmarks =  JSON.parse(localStorage.getItem('bookmarks'));
        // add new bookmark to array
        bookmarks.push(bookmark);
        //set to local storage
        localStorage.setItem('bookmarks', JSON.stringify(bookmarks));
    }

    //Clear form after submitting
    document.getElementById('myForm').reset();
    //refetch the bookmarks
    fetchBookmarks();
    //prevent form from submitting
    e.preventDefault();

}

//delete bookmark
function deleteBookmark(url){
    //get bookmarks from local storage
    var bookmarks =  JSON.parse(localStorage.getItem('bookmarks'));
    //loop through the bookmarks
    for(var i=0; i < bookmarks.length; i++ )
    {
        if(bookmarks[i].url == url)
        {
            //remove the bookmark from array
            bookmarks.splice(i,1);
        }
    }
    //set to local storage
    localStorage.setItem('bookmarks', JSON.stringify(bookmarks));
    //reset the book marks in the local storage
    fetchBookmarks();
   }

//fetch bookmark
function fetchBookmarks(){

    //get bookmarks from local storage
    var bookmarks =  JSON.parse(localStorage.getItem('bookmarks'));

     if (!bookmarks) { return; }
    //get output class
    var bookmarksResults =  document.getElementById('bookmarksResults');

    //build output
    bookmarksResults.innerHTML='';

    for(var i=0; i < bookmarks.length; i++ )
    {
        var name = bookmarks[i].name;
        var url = bookmarks[i].url;
        bookmarksResults.innerHTML+= '<div class="well">'+
                           '<h4>'+name+
                           '<button id="visit" name="visit" onclick="location.href=\''+addhttp(url)+'\'">Visit</button>'+
                           '<button id="remove" name="remove" onclick="deleteBookmark(\''+url+'\')">Remove</button>'+
                           '</h4>'+
                           '</div>';
    }
}

// validate form
function validateForm(siteName, siteURL) {
    var error = [];
    var expression = /[-a-zA-Z0-9@:%_\+.~#?&//=]{2,256}\.[a-z]{2,4}\b(\/[-a-zA-Z0-9@:%_\+.~#?&//=]*)?/gi;
    var regex = new RegExp(expression);

    //make sure there is a site name and url
    if(!siteName || !siteURL) {
        error.push('siteName or siteURL is missing!');
    }

    if (siteURL && !siteURL.match(regex)) {
        error.push('siteURL is not valid!');
    }

    return error;
}


function addhttp(url) {
  if (!/^(?:f|ht)tps?\:\/\//.test(url)) {
      url = "http://" + url;
  }
  return url;
}


