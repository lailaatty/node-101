var wrapperEl = document.querySelector('.myWrapper'),
    outputEl = wrapperEl.querySelector('.outputText'),
    inputEl = wrapperEl.querySelector('.dataTypeInput'),
    buttonEl = wrapperEl.querySelector('.enterButton');

buttonEl.addEventListener("click", function () {
    outputEl.innerText = howItWorksInJs(inputEl.value.toLowerCase());
});

